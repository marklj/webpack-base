class FakeService {
  constructor() {
    this.data = [
      { name: 'testing', value: 123 },
      { name: 'another thing', value: 987 },
    ];
  }
  getData() {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve(this.data);
      }, 1500);
    });
  }
}

export default FakeService;
