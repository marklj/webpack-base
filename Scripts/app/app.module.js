import angular from 'angular';
import uiRouter from '@uirouter/angularjs';

import angularSanitize from 'angular-sanitize';
import oclazyload from 'oclazyload';
import appState from './app.state';

import App from './app';
import fakeService from './shared/services/fakeService';

// import './polyfills';

angular.module('app', [
  uiRouter,
  angularSanitize,
  oclazyload,
  appState.name,
]).component('app', App)
  .service('FakeService', fakeService);
