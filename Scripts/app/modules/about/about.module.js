import angular from 'angular';
import AboutController from './aboutController';

export const name = 'about';
export default angular.module(name, [])
  .controller('AboutController', AboutController);
