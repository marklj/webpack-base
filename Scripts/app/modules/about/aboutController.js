
class AboutController {
  constructor(FakeData) {
    this.fakeData = FakeData;
    this.message = 'This works!';
  }
}

AboutController.$inject = ['FakeData'];

export default AboutController;
