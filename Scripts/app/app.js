import '../style/index.css';
import template from './template.html';

class AppController {
  constructor($transitions) {
    this.showLoadingIndicator = false;
    this.listenToLoadingEvents($transitions);
  }

  listenToLoadingEvents($transitions) {
    $transitions.onStart({}, (transition) => {
      if (transition.to().resolve) {
        this.showLoadingIndicator = true;
      }
    });
    $transitions.onSuccess({}, (transition) => {
      if (transition.to().resolve) {
        this.showLoadingIndicator = false;
      }
    });
  }
}

AppController.$inject = ['$transitions'];

const AppComponent = {
  controller: AppController,
  controllerAs: 'appCtrl',
  template,
};

export default AppComponent;
