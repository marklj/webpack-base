import angular from 'angular';

import homeTemplate from './modules/home/home-template.html';

export default angular.module('Router', [])
  .config(['$locationProvider', '$urlRouterProvider', '$stateProvider', ($locationProvider, $urlRouterProvider, $stateProvider) => {
    $locationProvider.html5Mode({
      enabled: false,
      requireBase: false,
    });

    $urlRouterProvider.when('', '/');
    $urlRouterProvider.otherwise('/404');

    $stateProvider.state('home', {
      url: '/',
      template: homeTemplate,
    });

    $stateProvider.state('about', {
      url: '/about',
      controller: 'AboutController',
      controllerAs: 'aboutCtrl',
      templateProvider: () => import(/* webpackChunkName: "about" */ './modules/about/about-template.html').then(t => t),
      resolve: {
        loadModule: ['$ocLazyLoad', ($ocLazyLoad) => {
          import(/* webpackChunkName: "about" */ './modules/about/about.module').then((module) => {
            $ocLazyLoad.load({ name: module.name });
          });
        }],
        FakeData: ['FakeService', FakeService => FakeService.getData().then(data => data)],
      },
    });
  }]);
