# AngularJS + Webpack Base
This project has been modified to work well with the .Net MVC framework. You can easily convert the project into almost any project structure by changing the values in the `./build/paths.js` configuration file.

## Features
- AngularJS (ES6)
- Webpack
- Code splitting
- Angular-UI Routing
- PostCSS
- ESLint (Airbnb style guide)

## Setup
- Clone the repo and run `npm install`
- You can spin up a dev server with hot-reloading enabled by running `npm start`
- OR, in the case of using within a .Net MVC web app, you can simply watch the files for changes using the `npm run watch` and webpack will re-bundle your app in the background!

## Production
Production build are minified and gzipped. Most development builds can be way over 5mb in size, so obviously you should never use the non-production builds outside of a dev environment.

To create a production build:
- run `npm run build:prod`

Production and development builds are output to the `./Content` directory for compatibility with .Net MVC. This output path can be changed in the `./build/paths.js` configuration file.