const paths = require('./paths');
const webpack = require('webpack');
const ExtractTextWebpackPlugin = require('extract-text-webpack-plugin');
const ParallelUglifyPlugin = require('webpack-parallel-uglify-plugin');
const CompressionWebpackPlugin = require('compression-webpack-plugin');

const config = {
  output: {
    publicPath: '/',
  },
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.css/,
        use: ExtractTextWebpackPlugin.extract({
          use: ['css-loader', 'postcss-loader'],
          fallback: 'style-loader',
        }),
      },
    ],
  },
  plugins: [
    new webpack.IgnorePlugin(/^(mv|rim-raf|source-map-support)$/),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      filename: '[name].bundle.js',
    }),
    new ExtractTextWebpackPlugin('style.css'),
    new ParallelUglifyPlugin({
      // Optional regex, or array of regex to match file against. Only matching files get minified.
      // Defaults to /.js$/, any file ending in .js.
      // test,
      // include, // Optional regex, or array of regex to include in minification. Only matching files get minified.
      // exclude, // Optional regex, or array of regex to exclude from minification. Matching files are not minified.
      cacheDir: paths.cachePath, // Optional absolute path to use as a cache. If not provided, caching will not be used.
      // workerCount, // Optional int. Number of workers to run uglify. Defaults to num of cpus - 1 or asset count (whichever is smaller)
      sourceMap: true, // Optional Boolean. This slows down the compilation. Defaults to false.
      // uglifyJS: {
      // These pass straight through to uglify-js@3.
      // Cannot be used with uglifyES.
      // Defaults to {} if not neither uglifyJS or uglifyES are provided.
      // You should use this option if you need to ensure es5 support. uglify-js will produce an error message
      // if it comes across any es6 code that it can't parse.
      // },
      uglifyES: {
        // These pass straight through to uglify-es.
        // Cannot be used with uglifyJS.
        // uglify-es is a version of uglify that understands newer es6 syntax. You should use this option if the
        // files that you're minifying do not need to run in older browsers/versions of node.
      },
    }),
    new CompressionWebpackPlugin({
      asset: '[path].gz[query]',
      algorithm: 'gzip',
      test: /\.(js|html|css)$/,
      minRatio: 0.8,
    }),
    new webpack.NoEmitOnErrorsPlugin(),
  ],
};

module.exports = config;
