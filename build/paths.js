const path = require('path');


module.exports = {
  outputPath: path.resolve(__dirname, '../', 'Content'),
  publicPath: path.resolve(__dirname, '../Scripts', 'public'),
  sourcePath: path.resolve(__dirname, '../', 'Scripts'),
  cachePath: path.resolve(__dirname, '../', 'cache'),
};
