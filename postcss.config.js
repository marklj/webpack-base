require('postcss-easy-import');
require('postcss-nested');
require('cssnano');

module.exports = {
  plugins: {
    'postcss-easy-import': {},
    'postcss-nested': {},
    autoprefixer: { browsers: ['last 2 versions'] },
    cssnano: {
      preset: 'default',
    },
  },
}
;