const commonConfig = require('./build/webpack.common');
const webpackMerge = require('webpack-merge');

module.exports = (env) => {
    const addons = (addonsArg) => {
        var addons = []
            .concat.apply([], [addonsArg])
            .filter(Boolean);
    
        return addons.map((addonName) => require(`./build/addons/webpack.${addonName}.js`));
    }

    console.log(env);
    const envConfig = require(`./build/webpack.${env.env}.js`);
    const mergedConfig = webpackMerge(commonConfig, envConfig, ...addons(env.addons));

    console.log(mergedConfig);
    return mergedConfig;
}